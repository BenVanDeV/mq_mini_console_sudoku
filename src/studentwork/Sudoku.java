package studentwork;

import processing.core.*;
import processing.data.*;

public class Sudoku extends mqapp.MQApp {

    public String name() {return "Sudoku";}
    public String author() { return "Benjamin van de Vorstenbosch"; }
    public String description() { return "9 unique rows, columns and groups!";}

    //Sudoku boards source: https://www.kaggle.com/bryanpark/sudoku?select=sudoku.csv
    Table puzzles;

    int numPuzzles = 10000;

    int[][] puzzle = new int[9][9];
    int[][] board = new int[9][9];
    int[][] solution = new int[9][9];

    IntList boardNumbers = new IntList();

    int gameSize, lineSplit;

    PVector gameOffset = new PVector();

    int backgroundColor = color(185, 185, 185);
    int boardColor = color(255);
    int highlightColor = color(173, 203, 227);
    int defaultNumberColor = color(0);
    int inputNumberColor = color(75, 134, 180);
    int incorrectNumber = color(255, 0, 0);

    PVector selectedIndex = new PVector(0, 0);

    boolean cheats = true;
    boolean canMove = true;
    boolean canPress = true;
    boolean gameOver = false;

    boolean showingConfirmation = false;
    boolean resetBoardConfirmation = false;
    boolean newBoardConfirmation = false;

    public void setup() {
        size(displayWidth, displayHeight);
        initialiseVariables();

        puzzles = loadTable("../../data/sudoku/sudoku-puzzles.csv", "header");
        loadBoard(true);

        background(backgroundColor);
        renderGame();
    }

    void initialiseVariables() {
        gameSize = min(displayWidth, displayHeight);
        gameSize = (int)(gameSize*0.9);
        gameOffset.x = (displayWidth - gameSize)/2;
        gameOffset.y = (displayHeight - gameSize)/2;

        lineSplit = gameSize/9;
    }

    public void draw() {
        gameOver = checkGameOver();

        if (!showingConfirmation && gameOver) {
            drawGameOver();
        }
    }

    boolean checkGameOver() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (board[i][j] != solution[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public void keyPressed() {
        if(canPress) {
            checkBoardSettingsKeys();

            if(canMove && !gameOver) {
                checkMovementKeys();
            }
            else {
                if(key == BACKSPACE && showingConfirmation) {
                    if(!gameOver) {
                        canMove = true;
                        renderGame();
                    }
                    resetConfirmations();
                }
            }
            canPress = false;
        }
    }

    public void keyReleased() {
        canPress = true;
    }

    void checkBoardSettingsKeys() {
        if(key == 'n') {
            if(newBoardConfirmation) {
                //Create new board
                rebuildBoard(true);
            }
            else if(!resetBoardConfirmation) {
                //Confirm new board request
                drawConfirmation(false);
                newBoardConfirmation = true;
            }
        }

        if(key == 'r') {
            if(resetBoardConfirmation) {
                //Reset current board
                rebuildBoard(false);
            }
            else if(!newBoardConfirmation) {
                //Confirm reset board request
                drawConfirmation(true);
                resetBoardConfirmation = true;
            }
        }
    }

    void checkMovementKeys() {
        if(key == CODED) {
            switch(keyCode) {
                case UP:
                    selectedIndex.y = (selectedIndex.y != 0 ? selectedIndex.y - 1 : selectedIndex.y);
                    break;
                case LEFT:
                    selectedIndex.x = (selectedIndex.x != 0 ? selectedIndex.x - 1 : selectedIndex.x);
                    break;
                case DOWN:
                    selectedIndex.y = (selectedIndex.y != (puzzle.length - 1) ? selectedIndex.y + 1 : selectedIndex.y);
                    break;
                case RIGHT:
                    selectedIndex.x = (selectedIndex.x != (puzzle.length - 1) ? selectedIndex.x + 1 : selectedIndex.x);
                    break;
            }
        }
        else {
            switch(key) {
                case BACKSPACE:
                    changeNumber((int)(selectedIndex.x), (int)(selectedIndex.y), 0);
                    break;
                case DELETE:
                    changeNumber((int)(selectedIndex.x), (int)(selectedIndex.y), 0);
                    break;
                case 'c':
                    completeBoard();
                    break;
                default:
                    fillNumber();
                    break;
            }
        }
        renderGame();
    }

    void rebuildBoard(boolean type) {
        //True: New Board
        //False: Reset board

        resetConfirmations();
        gameOver = false;
        canMove = true;

        loadBoard(type);
    }

    void resetConfirmations() {
        showingConfirmation = false;
        resetBoardConfirmation = false;
        newBoardConfirmation = false;
    }

    void fillNumber() {
        //Converts key to number. Taking away 48 lines up 0-9 correctly.
        int number = key - 48;
        if(number >= 0 && number <= 9) {
            changeNumber((int)(selectedIndex.x), (int)(selectedIndex.y), number);
        }
    }

    void changeNumber(int x, int y, int newNumber) {
        if(puzzle[y][x] == 0) {
            board[y][x] = (board[y][x] == newNumber ? 0 : newNumber);

            renderGame();
        }
    }

    void completeBoard() {
        if(cheats) {
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board.length; j++) {
                    board[i][j] = solution[i][j];
                    renderGame();
                }
            }
        }
    }

    void renderGame() {
        drawBackbox();
        drawHighlights();
        drawBoard();
        drawNumbers();
    }

    void loadBoard(boolean newBoard) {
        if(newBoard) {
            int newNumber = (int)(random(numPuzzles));

            //Ensure no puzzles are reused
            while(boardNumbers.hasValue(newNumber)) {
                newNumber = (int)(random(numPuzzles));
            }

            boardNumbers.append(newNumber);
        }

        TableRow row = puzzles.getRow(boardNumbers.get(boardNumbers.size()-1));

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                puzzle[i][j] = row.getString("quizzes").charAt(j + 9*i) - 48;
                solution[i][j] = row.getString("solutions").charAt(j + 9*i) - 48;
                board[i][j] = puzzle[i][j];
            }
        }

        renderGame();
        gameOver = false;
    }

    void drawBackbox() {
        fill(boardColor);
        rectMode(CORNER);
        rect(gameOffset.x, gameOffset.y, gameSize, gameSize);
    }

    void drawBoard() {
        for(int i = 0; i <= board.length; i++) {
            strokeWeight((i % 3 == 0) ? 3 : 1);
            stroke((i % 3 == 0) ? 0 : 30);
            line(gameOffset.x, gameOffset.y+lineSplit*i, width-gameOffset.x, gameOffset.y+lineSplit*i);
            line(gameOffset.x+lineSplit*i, gameOffset.y, gameOffset.x+lineSplit*i, height-gameOffset.y);
        }
    }

    void drawNumbers() {
        for(int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if(board[j][i] != 0)
                {
                    fill((puzzle[j][i] == 0) ? inputNumberColor : defaultNumberColor);

                    textAlign(CENTER, TOP);
                    textSize(gameSize/9);
                    text(board[j][i], (float)(gameOffset.x+lineSplit*(i+0.5)), gameOffset.y+lineSplit*j);
                }
            }
        }
    }

    void drawHighlights() {
        fill(highlightColor);
        noStroke();

        PVector pos = getBoardPos((int)(selectedIndex.x), (int)(selectedIndex.y));
        rect(pos.x, pos.y, lineSplit, lineSplit);
    }

    PVector getBoardPos(int xIndex, int yIndex) {
        PVector pos = new PVector();
        pos.x = gameOffset.x + xIndex * lineSplit;
        pos.y = gameOffset.y + yIndex * lineSplit;

        return pos;
    }

    void drawGameOver() {
        drawSettingsBox();

        fill(0);
        textAlign(CENTER);
        textSize(gameSize/15);
        text("You Win!", displayWidth/2, displayHeight/2 - gameSize/24);

        textSize(gameSize/50);
        text("Press 'n' to start a new game.", displayWidth/2, displayHeight/2);
        text("Press 'r' to start over.", displayWidth/2, displayHeight/2 + gameSize/24);
    }

    void drawConfirmation(boolean type) {
        showingConfirmation = true;
        canMove = false;

        drawSettingsBox();

        fill(0);
        textAlign(CENTER);
        textSize(gameSize/25);
        text("Are you sure?", displayWidth/2, displayHeight/2 - gameSize/24);

        textSize(gameSize/50);
        if (type) {
            text("If you want to restart,\npress 'r' again", displayWidth/2, displayHeight/2);
        } else {
            text("If you want to start a new game,\npress 'n' again", displayWidth/2, displayHeight/2);
        }

        text("Press 'BACKSPACE' to cancel", displayWidth/2, displayHeight/2 + gameSize/8);
    }

    void drawSettingsBox() {
        stroke(0);
        strokeWeight(5);
        fill(highlightColor);
        rectMode(CENTER);
        rect(displayWidth/2, displayHeight/2, gameSize/3, gameSize/3);
    }
}
